﻿/* Resize Calculator, Form1.cs
 * Calculates the new width/height of an Image/Video within a given aspect ratio.
 * ©Kai Sackl, 31.08.2019
 */

using System;
using System.Windows.Forms;
using Rationals;

namespace ResizeCalculator
{
    public partial class Form1 : Form
    {
        private int oldWidth, oldHeight;

        public Form1()
        {
            InitializeComponent();
            Init();
        }

        private void Init()
        {
            oldWidth = 0;
            oldHeight = 0;
        }

        #region TextChanged Events
        private void TbxOldWidth_TextChanged(object sender, EventArgs e)
        {
            _ = int.TryParse(TbxOldWidth.Text, out oldWidth);

            FillAspectRatioTextboxes(ConvertToRational(oldWidth, oldHeight));
        }

        private void TbxOldHeight_TextChanged(object sender, EventArgs e)
        {
            _ = int.TryParse(TbxOldHeight.Text, out oldHeight);

            FillAspectRatioTextboxes(ConvertToRational(oldWidth, oldHeight));
        }

        private void TbxNewWidth_TextChanged(object sender, EventArgs e)
        {
            if (((TextBox)sender).Modified && double.TryParse(TbxNewWidth.Text, out double resultWidth) && int.TryParse(TbxRatioX.Text, out int resultRatioX) && int.TryParse(TbxRatioY.Text, out int resultRatioY))
            {
                int newHeight = (int)(resultWidth / CalcDecimal(resultRatioX, resultRatioY));

                TbxNewHeight.Text = newHeight.ToString();
            }
        }

        private void TbxNewHeight_TextChanged(object sender, EventArgs e)
        {
            if (((TextBox)sender).Modified && double.TryParse(TbxNewHeight.Text, out double resultHeight) && int.TryParse(TbxRatioX.Text, out int resultRatioX) && int.TryParse(TbxRatioY.Text, out int resultRatioY))
            {
                int newWidth = (int)(resultHeight * CalcDecimal(resultRatioX, resultRatioY));

                TbxNewWidth.Text = newWidth.ToString();
            }
        }
        #endregion

        #region KeyPress Events
        private void TbxOldWidth_KeyPress(object sender, KeyPressEventArgs e)
        {
            CheckIfOnlyNumbers(e);
        }

        private void TbxOldHeight_KeyPress(object sender, KeyPressEventArgs e)
        {
            CheckIfOnlyNumbers(e);
        }

        private void TbxRatioX_KeyPress(object sender, KeyPressEventArgs e)
        {
            CheckIfOnlyNumbers(e);
        }

        private void TbxRatioY_KeyPress(object sender, KeyPressEventArgs e)
        {
            CheckIfOnlyNumbers(e);
        }

        private void TbxNewWidth_KeyPress(object sender, KeyPressEventArgs e)
        {
            CheckIfOnlyNumbers(e);
        }

        private void TbxNewHeight_KeyPress(object sender, KeyPressEventArgs e)
        {
            CheckIfOnlyNumbers(e);
        }
        #endregion

        #region Helper Methods
        /// <summary>
        /// Checks if only numbers are typed in the textboxes
        /// </summary>
        /// <param name="e"></param>
        private void CheckIfOnlyNumbers(KeyPressEventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(e.KeyChar.ToString(), "[^0-9\b]"))
                e.Handled = true;
        }

        /// <summary>
        /// Calculates the decimal of two integers, converts it to a rational and returns it
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns>A Rational Number</returns>
        private Rational ConvertToRational(int width, int height)
        {
            if (!string.IsNullOrWhiteSpace(width.ToString()) && !string.IsNullOrWhiteSpace(height.ToString()) && width != 0 && height != 0)
            {
                double dec = CalcDecimal(width, height);
                Rational rat = (Rational) dec;
                return rat;
            }
            else
            {
                return (Rational) 1/1;
            }
        }

        /// <summary>
        /// Fills the Aspect Ratio Textboxes
        /// </summary>
        /// <param name="rat">A Rational Number</param>
        private void FillAspectRatioTextboxes(Rational rat)
        {
            string[] splitedRational = rat.ToString().Split('/');

            if (splitedRational.Length >= 2)
            {
                TbxRatioX.Text = splitedRational[0];
                TbxRatioY.Text = splitedRational[1];
            }
            else
            {
                TbxRatioX.Text = "";
                TbxRatioY.Text = "";
            }
        }

        /// <summary>
        /// Returns the decimal of two integers
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns>Decimal of two integers</returns>
        private double CalcDecimal(int x, int y) => (double)x / y;
        #endregion
    }
}
