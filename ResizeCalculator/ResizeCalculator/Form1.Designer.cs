﻿namespace ResizeCalculator
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.TbxOldWidth = new System.Windows.Forms.TextBox();
            this.LblOldWidth = new System.Windows.Forms.Label();
            this.LblOldHeight = new System.Windows.Forms.Label();
            this.TbxOldHeight = new System.Windows.Forms.TextBox();
            this.LblAspectRatio = new System.Windows.Forms.Label();
            this.TbxRatioX = new System.Windows.Forms.TextBox();
            this.TbxRatioY = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.LblNewWidth = new System.Windows.Forms.Label();
            this.LblNewHeight = new System.Windows.Forms.Label();
            this.TbxNewWidth = new System.Windows.Forms.TextBox();
            this.TbxNewHeight = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // TbxOldWidth
            // 
            this.TbxOldWidth.Location = new System.Drawing.Point(12, 33);
            this.TbxOldWidth.Name = "TbxOldWidth";
            this.TbxOldWidth.Size = new System.Drawing.Size(105, 20);
            this.TbxOldWidth.TabIndex = 0;
            this.TbxOldWidth.TextChanged += new System.EventHandler(this.TbxOldWidth_TextChanged);
            this.TbxOldWidth.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TbxOldWidth_KeyPress);
            // 
            // LblOldWidth
            // 
            this.LblOldWidth.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblOldWidth.Location = new System.Drawing.Point(29, 10);
            this.LblOldWidth.Name = "LblOldWidth";
            this.LblOldWidth.Size = new System.Drawing.Size(75, 20);
            this.LblOldWidth.TabIndex = 1;
            this.LblOldWidth.Text = "Old Width";
            // 
            // LblOldHeight
            // 
            this.LblOldHeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblOldHeight.Location = new System.Drawing.Point(135, 10);
            this.LblOldHeight.Name = "LblOldHeight";
            this.LblOldHeight.Size = new System.Drawing.Size(82, 20);
            this.LblOldHeight.TabIndex = 2;
            this.LblOldHeight.Text = "Old Height";
            // 
            // TbxOldHeight
            // 
            this.TbxOldHeight.Location = new System.Drawing.Point(123, 33);
            this.TbxOldHeight.Name = "TbxOldHeight";
            this.TbxOldHeight.Size = new System.Drawing.Size(105, 20);
            this.TbxOldHeight.TabIndex = 3;
            this.TbxOldHeight.TextChanged += new System.EventHandler(this.TbxOldHeight_TextChanged);
            this.TbxOldHeight.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TbxOldHeight_KeyPress);
            // 
            // LblAspectRatio
            // 
            this.LblAspectRatio.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblAspectRatio.Location = new System.Drawing.Point(73, 63);
            this.LblAspectRatio.Name = "LblAspectRatio";
            this.LblAspectRatio.Size = new System.Drawing.Size(92, 20);
            this.LblAspectRatio.TabIndex = 4;
            this.LblAspectRatio.Text = "Aspect Ratio";
            // 
            // TbxRatioX
            // 
            this.TbxRatioX.Location = new System.Drawing.Point(77, 86);
            this.TbxRatioX.Name = "TbxRatioX";
            this.TbxRatioX.Size = new System.Drawing.Size(36, 20);
            this.TbxRatioX.TabIndex = 5;
            this.TbxRatioX.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TbxRatioX_KeyPress);
            // 
            // TbxRatioY
            // 
            this.TbxRatioY.Location = new System.Drawing.Point(123, 86);
            this.TbxRatioY.Name = "TbxRatioY";
            this.TbxRatioY.Size = new System.Drawing.Size(36, 20);
            this.TbxRatioY.TabIndex = 6;
            this.TbxRatioY.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TbxRatioY_KeyPress);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(113, 85);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(10, 20);
            this.label1.TabIndex = 7;
            this.label1.Text = ":";
            // 
            // LblNewWidth
            // 
            this.LblNewWidth.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblNewWidth.Location = new System.Drawing.Point(29, 134);
            this.LblNewWidth.Name = "LblNewWidth";
            this.LblNewWidth.Size = new System.Drawing.Size(84, 20);
            this.LblNewWidth.TabIndex = 8;
            this.LblNewWidth.Text = "New Width";
            // 
            // LblNewHeight
            // 
            this.LblNewHeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblNewHeight.Location = new System.Drawing.Point(137, 134);
            this.LblNewHeight.Name = "LblNewHeight";
            this.LblNewHeight.Size = new System.Drawing.Size(91, 20);
            this.LblNewHeight.TabIndex = 9;
            this.LblNewHeight.Text = "New Height";
            // 
            // TbxNewWidth
            // 
            this.TbxNewWidth.Location = new System.Drawing.Point(18, 157);
            this.TbxNewWidth.Name = "TbxNewWidth";
            this.TbxNewWidth.Size = new System.Drawing.Size(105, 20);
            this.TbxNewWidth.TabIndex = 10;
            this.TbxNewWidth.TextChanged += new System.EventHandler(this.TbxNewWidth_TextChanged);
            this.TbxNewWidth.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TbxNewWidth_KeyPress);
            // 
            // TbxNewHeight
            // 
            this.TbxNewHeight.Location = new System.Drawing.Point(129, 157);
            this.TbxNewHeight.Name = "TbxNewHeight";
            this.TbxNewHeight.Size = new System.Drawing.Size(105, 20);
            this.TbxNewHeight.TabIndex = 11;
            this.TbxNewHeight.TextChanged += new System.EventHandler(this.TbxNewHeight_TextChanged);
            this.TbxNewHeight.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TbxNewHeight_KeyPress);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(240, 191);
            this.Controls.Add(this.TbxNewHeight);
            this.Controls.Add(this.TbxNewWidth);
            this.Controls.Add(this.LblNewHeight);
            this.Controls.Add(this.LblNewWidth);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TbxRatioY);
            this.Controls.Add(this.TbxRatioX);
            this.Controls.Add(this.LblAspectRatio);
            this.Controls.Add(this.TbxOldHeight);
            this.Controls.Add(this.LblOldHeight);
            this.Controls.Add(this.LblOldWidth);
            this.Controls.Add(this.TbxOldWidth);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Resize Calculator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TbxOldWidth;
        private System.Windows.Forms.Label LblOldWidth;
        private System.Windows.Forms.Label LblOldHeight;
        private System.Windows.Forms.TextBox TbxOldHeight;
        private System.Windows.Forms.Label LblAspectRatio;
        private System.Windows.Forms.TextBox TbxRatioX;
        private System.Windows.Forms.TextBox TbxRatioY;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label LblNewWidth;
        private System.Windows.Forms.Label LblNewHeight;
        private System.Windows.Forms.TextBox TbxNewWidth;
        private System.Windows.Forms.TextBox TbxNewHeight;
    }
}

